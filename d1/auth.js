//JSON web token standard for sending info between our app in a secure manner
// allows access to methods that will help us to create a json web token
const jwt = require("jsonwebtoken");
const secret = "CrushAkoNgCrushKo"; //only the system knows this code/secret keyword; part of the signature;

/* 
  - JWT is a way of securely passing info from the server to the frontend or to the other parts of the server 
  - the info is kept secure through the use of the secret code 
  - only the system that knows the secret code that can decode the encrypted info 
*/

/* 
Token creation
  - need to say to the server the needed data to create a web token
  - analogy: pack the gift and provide a lock with the secret code as the key 
*/

module.exports.createAccessToken = (user) => {
  //data will be received from the registration form
  //when the users log in, a token will be created with user's information
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  //generates the token using the form data and the secret code with no additional options provided
  return jwt.sign(data, secret, {}); //can put options in the curly brackets
};

//Token Verification
module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;

  if (typeof token !== "undefined") {
    console.log(token);

    //"bearer + jwt code" - want to remove bearer and just get the jwt code
    token = token.slice(7, token.length);

    // validate the token using the 'verify' method
    return jwt.verify(token, secret, (err, data) => {
      //if jwt is not valid
      if (err) {
        return res.send({ auth: "failed" });
      } else {
        //allows the application to proceed with the next middleware function/callback function in the route
        next();
      }
    });
  }
  //token does not exist
  else {
    return res.send({ auth: "failed" });
  }
};

//Token decryption
module.exports.decode = (token) => {
  //Token received and is not undefined
  if (typeof token !== "undefined") {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        //decode method is used to obtain info from the jwt
        //complete: true option allows to return additional info from the jwt
        //returns an object with access to the "payload" property which contains user information stored when the token was generated
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  } else {
    return null;
  }
};
