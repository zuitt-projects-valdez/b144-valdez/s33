const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/user");
//routes for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
  userController.checkEmailExists(req.body).then((result) => res.send(result));
});

//post and not get because we are getting something/looking for req.body

//ROUTES FOR USER REGISTRATION
router.post("/register", (req, res) => {
  userController.registerUser(req.body).then((result) => res.send(result));
});

//routes for authenticating a user
router.post("/login", (req, res) => {
  userController.loginUser(req.body).then((result) => res.send(result));
});

//Activity: routes for retrieving user details
/* router.post("/details", (req, res) => {
  userController.getProfile(req.body).then((result) => {
    res.send(result);
    //userController.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController))
  });
}); */
router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  userController
    .getProfile({ userId: userData.id }) //the decoded jwt from the header
    .then((resultFromController) => res.send(resultFromController));
});

//Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
  let data = {
    userId: req.body.userId,
    courseId: req.body.courseId,
  };
  userController.enroll(data).then((result) => res.send(result));
});

module.exports = router;
